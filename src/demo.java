
import org.apache.log4j.Logger;

public class demo 
{
	final static Logger LOG= Logger.getLogger(demo.class);
			
	
	public static void main(String[] args) 
	{
		LOG.trace("tracing");
		LOG.debug("debugging");
		
		LOG.error("error");
		LOG.warn("warning");
		LOG.info("information");
		LOG.fatal("fatal");
	}

}
